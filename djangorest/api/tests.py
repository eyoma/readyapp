from django.test import TestCase

# Create your tests here.
from django.test import TestCase
from .models import Dog


class ModelTestCase(TestCase):
    """This class defines the test suite for the dog model."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.dog_name = "Alexa2"
        self.dog = Dog(dog_name=self.dog_name)

    def test_model_can_create_a_dog(self):
        """Test the dog model can create a dog."""
        old_count = Dog.objects.count()
        self.dog.save()
        new_count = Dog.objects.count()
        self.assertNotEqual(old_count, new_count)