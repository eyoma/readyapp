# api/serializers.py

from rest_framework import serializers
from .models import Dog


class DogSerializer(serializers.ModelSerializer):
    """Serializer to map the Model instance into JSON format."""

    class Meta:
        """Meta class to map serializer's fields with the model fields."""
        model = Dog
        fields = ('dog_name','gender','breed','birth','dominant_color','secondary_color','third_color','spayed_or_neutered','guard_or_trained','borough','zip_code')
