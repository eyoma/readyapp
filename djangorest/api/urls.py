
from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from .views import CreateView, CountView

urlpatterns = {
    url(r'^createdog/$', CreateView.as_view(), name="create"),
    url(r'^count$', CountView.as_view(), name="counter"),
}

urlpatterns = format_suffix_patterns(urlpatterns)
