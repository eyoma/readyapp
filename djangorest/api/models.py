from django.db import models

# Create your models here.

class Dog(models.Model):
    dog_name = models.CharField(max_length=255)
    gender = models.CharField(max_length=255)
    breed = models.CharField(max_length=255)
    birth = models.CharField(max_length=255)
    dominant_color = models.CharField(max_length=255)
    secondary_color = models.CharField(max_length=255)
    third_color = models.CharField(max_length=255)
    spayed_or_neutered = models.CharField(max_length=255)
    guard_or_trained = models.CharField(max_length=255)
    borough = models.CharField(max_length=255)
    zip_code = models.CharField(max_length=255)

    def __str__(self):
        """Return a human readable representation of the model instance."""
        return "{}".format(self.dog_name)
