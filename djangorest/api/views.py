from django.shortcuts import render

# Create your views here.
from rest_framework import generics
from .serializers import DogSerializer
from .models import Dog
from rest_framework.views import APIView
from django.http import Http404
from rest_framework.response import Response
from rest_framework import status
from rest_framework.renderers import JSONRenderer



class CreateView(generics.ListCreateAPIView):
    """This class defines the create behavior of our rest api."""
    queryset = Dog.objects.all()
    serializer_class = DogSerializer

    def perform_create(self, serializer):
        """Save the post data when creating a new bucketlist."""
        serializer.save()

        

class CountView(APIView):
    """This class defines the create behavior of our rest api."""
 
    serializer_class = DogSerializer
    renderer_classes = (JSONRenderer, )
    
    def get(self, request, format=None):
        queryset = Dog.objects.all()

        validItems=['dog_name' ,'gender' ,'breed' ,'birth' ,'dominant_color' ,'secondary_color' ,'third_color' ,'spayed_or_neutered' ,'guard_or_trained','borough' ,'zip_code']
        unknown=[]
        print('items', self.request.query_params.keys() )
        for item in self.request.query_params.keys():
            if item not in validItems:
                unknown.append(item)

        if len(unknown)>0:
            unknown.sort()
            return Response({"unknown fields": unknown}, status=status.HTTP_400_BAD_REQUEST)
        else:
            dog_name = self.request.query_params.get('dog_name', None)
            gender = self.request.query_params.get('gender', None)
            breed = self.request.query_params.get('breed', None)
            birth = self.request.query_params.get('birth', None)
            dominant_color = self.request.query_params.get('dominant_color', None)
            secondary_color = self.request.query_params.get('secondary_color', None)
            third_color = self.request.query_params.get('third_color', None)
            spayed_or_neutered = self.request.query_params.get('spayed_or_neutered', None)
            guard_or_trained = self.request.query_params.get('guard_or_trained', None)
            borough = self.request.query_params.get('borough', None)
            zip_code = self.request.query_params.get('zip_code', None)

            if dog_name is not None:
                queryset=queryset.filter(dog_name = dog_name.capitalize())
            if gender is not None:
                queryset=queryset.filter(gender = gender.upper())
            if breed is not None:
                queryset=queryset.filter(breed = breed)
            if birth is not None:
                queryset=queryset.filter(birth = birth.capitalize())
            if dominant_color is not None:
                queryset=queryset.filter(dominant_color = dominant_color.upper())
            if secondary_color is not None:
                queryset=queryset.filter(secondary_color = secondary_color.upper())
            if third_color is not None:
                queryset=queryset.filter(third_color = third_color.upper())
            if spayed_or_neutered is not None:
                queryset=queryset.filter(spayed_or_neutered = spayed_or_neutered.capitalize())
            if guard_or_trained is not None:
                queryset=queryset.filter(guard_or_trained = guard_or_trained.capitalize())
            if borough is not None:
                queryset=queryset.filter(borough = borough.capitalize())
            if zip_code is not None:
                queryset=queryset.filter(zip_code = zip_code)

            print (queryset.count())
            return Response({"count": queryset.count()})
            # serializer = DogSerializer(queryset, many=True) #Dont want to print out all the dogs
            # return Response(serializer.data)
