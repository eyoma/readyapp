
import sys, os
# sys.path.append(your_djangoproject_home)
# os.environ['DJANGO_SETTINGS_MODULE'] ='djangorest.settings'
from api.models import Dog

import csv

csv_filepathname="/Users/eyoma/Desktop/Dogs.csv"

your_djangoproject_home="/Users/eyoma/Desktop/readypython"

dataReader = csv.reader(open(csv_filepathname), delimiter=',', quotechar='"')

for row in dataReader:
    newDog = Dog()
    newDog.dog_name=row[0]
    newDog.gender=row[1]
    newDog.breed=row[2]
    newDog.birth=row[3]
    newDog.dominant_color=row[4]
    newDog.secondary_color=row[5]
    newDog.third_color=row[6]
    newDog.spayed_or_neutered=row[7]
    newDog.guard_or_trained=row[8]
    newDog.borough=row[9]
    newDog.zip_code=row[10]
    newDog.save()